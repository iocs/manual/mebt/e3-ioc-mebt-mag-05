require essioc
require caenelsmagnetps, 1.2.2+1
require magnetps, 1.1.0

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet("IOCNAME", "MEBT-010:SC-IOC-009")
epicsEnvSet("IOCDIR", "MEBT-010_SC-IOC-009")
iocshLoad("$(essioc_DIR)/common_config.iocsh")
#
############################################################################
# MEBT Magnets IP addresses
############################################################################
iocshLoad("$(E3_CMD_TOP)/fastps_ips.iocsh")
#
#
############################################################################
# Load conversion break point table
############################################################################
dbLoadDatabase("MEBT-010_BMD-CV-001.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-002.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-003.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-004.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-005.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-006.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-007.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-008.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-009.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-010.dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("MEBT-010_BMD-CV-011.dbd", "$(magnetps_DB)/", "")
updateMenuConvert
#
############################################################################
# MEBT Corrector CV-001
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-001")
epicsEnvSet(PSPORT, "CV-01")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV1_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-001")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-002
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-002")
epicsEnvSet(PSPORT, "CV-02")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV2_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-002")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-003
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-003")
epicsEnvSet(PSPORT, "CV-03")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV3_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-003")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-004
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-004")
epicsEnvSet(PSPORT, "CV-04")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV4_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-004")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-005
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-005")
epicsEnvSet(PSPORT, "CV-05")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV5_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-005")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-006
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-006")
epicsEnvSet(PSPORT, "CV-06")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV6_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-006")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-007
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-007")
epicsEnvSet(PSPORT, "CV-07")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV7_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-007")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-008
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-008")
epicsEnvSet(PSPORT, "CV-08")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV8_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-008")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-009
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-009")
epicsEnvSet(PSPORT, "CV-09")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV9_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-009")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-010
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-010")
epicsEnvSet(PSPORT, "CV-10")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV10_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-010")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")
#
#
############################################################################
# MEBT Corrector CV-011
############################################################################
epicsEnvSet(PSUNIT, "PwrC-PSCV-011")
epicsEnvSet(PSPORT, "CV-11")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=PS$(PSPORT), IP_ADDR=$(PSCV11_IP), P=MEBT-010:, R=$(PSUNIT):")
#
epicsEnvSet(CONVERSIONTABLE, "MEBT-010_BMD-CV-011")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=MEBT-010:, R=$(PSUNIT):, READCURPV=Cur-R, BPT=$(CONVERSIONTABLE), EGU=T, ADEL=0")

# Load specific configurations for MEBT CV 06
dbLoadRecords("./db/mebt_cv_06.db","P=MEBT-010:")

asSetFilename("$(essioc_DIR)/mebtcv06.acf")
asSetSubstitutions("$(ASG_SET_SUBSTITUTIONS_STRINGS)")

#- Call iocInit to start the IOC
iocInit()
